﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;


namespace Scamps_ePortal_Utility
{
    class FireFoxSeleniumDriver : SeleniumDriver
    {

        
        IWebDriver driver = null;
        public FireFoxSeleniumDriver() : base()
        { 
           
        
        }
        public override void SetDriver() 
        {
            FirefoxProfile profile = (new FirefoxProfileManager()).GetProfile("default");
            driver = new FirefoxDriver(profile);
            

            // driver = new ChromeDriver(new ChromeOptions() { 
                // BinaryLocation = @"C:\Users\ch178188\AppData\Local\Chromium\Application\chrome.exe" });
        
        }

        public override IWebDriver GetDriver()
        {
            return driver;
        }


    }
}
