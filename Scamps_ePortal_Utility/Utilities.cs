﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;


namespace Scamps_ePortal_Utility
{
   public class Utilities
    {
       public const string BASE_URL = "http://epdev2.ircda.org/";
       //public const string TEST_WEBSITE_URL = "http://swdev1.ircda.org/login";
       public static string TEST_WEBSITE_URL = BASE_URL+"login";

       // 1=Chrome
       // 2=IE
       // 3=Safari
       // 4=Opera
       // 5=FireFox
       
       //
       public static void JSClick(OpenQA.Selenium.Remote.RemoteWebDriver browser, IWebElement element)
       {
           browser.ExecuteScript("arguments[0].click();", element);
       }

       // deleted 8_31_16 public static RemoteWebDriver GetWebDriver(int BrowserType)
       public static IWebDriver GetWebDriver (int BrowserType) // Added 8_31_16
       {
            //Delete 8_31_2016// RemoteWebDriver driver = null;
            IWebDriver driver = null; /*Added 8_31_2016*/
            SeleniumDriver myDriver = null; /*Added 8_31_2016*/            
            switch (BrowserType)
           {
               case 1:
                    //driver = new ChromeSeleniumDriver();
                    // Delete 8_31_2016//  driver = new ChromeDriver(new ChromeOptions() { BinaryLocation = @"C:\Users\ch178188\AppData\Local\Chromium\Application\chrome.exe" });
                    myDriver = new ChromeSeleniumDriver(); /*Added 8_31_2016*/
                    driver = myDriver.GetDriver(); /*Added 8_31_2016*/
                    break;
              
              // case 2:
                    //driver = new IESeleniumDriver();
                    //commented9-16-16-->string path = @"C:\Users\ch178188\AppData\Local\IEDriver"; InternetExplorerOptions options = new InternetExplorerOptions();
                    //commented9-16-16-->driver = new InternetExplorerDriver(path, options);
                    //myDriver1 = new IESeleniumDriver(); /*Added 9_16_2016*/ // not working 
                    //driver = myDriver1.GetDriver(); /*Added 9_16_2016*/     // not working 
                    //break;
             
            /* case 3:
                   driver = new SafariSeleniumDriver();
                   break;
               case 4:
                   driver = new OperaSeleniumDriver();
                   break;
            */      
               //case 5:

                    //driver = new FireFoxSeleniumDriver();
                    //FirefoxProfile profile = (new FirefoxProfileManager()).GetProfile("default");
                    //driver = new FirefoxDriver(profile);
                    //break;

           }
           return driver;
       }


        static void Main(string[] args)
        {

        }
    }
}
