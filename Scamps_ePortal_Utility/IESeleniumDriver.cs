﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using System.Threading.Tasks;

namespace Scamps_ePortal_Utility
{
    class IESeleniumDriver : SeleniumDriver
    {

        
        IWebDriver driver = null;
        public IESeleniumDriver() : base()
        { 
           
        
        }
        public override  void SetDriver() 
        {
            //commented 9-16-16--> string path = @"C:\Users\ch178188\AppData\Local\IEDriver";
            //commented 9-16-16-->InternetExplorerOptions options = new InternetExplorerOptions();

            //commented 9-16-16--> driver = new InternetExplorerDriver(path,options);

            //System.Threading.Thread.Sleep(5000);

            // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));

            driver = new InternetExplorerDriver(); // Added 9_16_16

        }

        public override IWebDriver GetDriver()
        {
            return driver;
        }


    }
}
