﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Scamps_ePortal_Utility
{
    class ChromeSeleniumDriver: SeleniumDriver
        


    {
        IWebDriver driver = null;
        public ChromeSeleniumDriver() : base()
        { 
           
        
        }
        public override void SetDriver() 
        {
            /* commented-->8_31_16 driver = new ChromeDriver(new ChromeOptions() { 
                BinaryLocation = @"C:\Users\ch178188\AppData\Local\Chromium\Application\chrome.exe" });
            */
            driver = new ChromeDriver(); // Added 8_31_16

        }

        public override IWebDriver GetDriver()
        {
            return driver;
        }
    }

}
