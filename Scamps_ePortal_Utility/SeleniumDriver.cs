﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;

namespace Scamps_ePortal_Utility
{
  
    public abstract class SeleniumDriver
    {
       
        public SeleniumDriver()
        {
            SetDriver();
        }

        public abstract void SetDriver();
        public abstract IWebDriver GetDriver();
        public IWebElement FindElementByName (String ElementName) 
        {
            return GetDriver().FindElement(By.Name(ElementName));
        }

        public IWebElement FindElement(String name)
        {
            IWebElement element = null;
            element = GetDriver().FindElement(By.ClassName(name));
            if (element == null)
            {
                element = GetDriver().FindElement(By.Name(name));
            }
            if (element == null)
            {
                element = GetDriver().FindElement(By.Id(name));
            }

            {
                element = GetDriver().FindElement(By.TagName(name));
            }

            {
                element = GetDriver().FindElement(By.CssSelector(name));
            }

            return element;


        }
        public IWebElement FindElementByClassName(String ClassName)
        {
            return GetDriver().FindElement(By.ClassName(ClassName));
        }


        public IWebElement FindElementById(String Id)
        {
            return GetDriver().FindElement(By.Id(Id)); 
        }


        // by finding by Link
        public IWebElement FindElementByLinkText(String LinkText)
        {
            return GetDriver().FindElement(By.LinkText(LinkText));
        }

        public IWebElement FindElementByXPath(String XPath)
        {
            return GetDriver().FindElement(By.XPath(XPath));

        }

        public IWebElement FindElementByType(String Value)
        {

            return GetDriver().FindElement(By.ClassName(Value));
        }
               
           
        public void CloseBrowser() 
        
        {
            GetDriver().Close();
            GetDriver().Dispose();
        }

        public void NavigateURL() 
        
        {
            GetDriver().Navigate().GoToUrl(Utilities.TEST_WEBSITE_URL);
        }

        public void dispose()
        {
            throw new NotImplementedException();
        }
    }
}
